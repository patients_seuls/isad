# Review Question

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Chapter 01](#chapter-01)
2. [Chapter 02](#chapter-02)
3. [Chapter 03](#chapter-03)
4. [Chapter 04](#chapter-04)
5. [Chapter 05](#chapter-05)
6. [Chapter 06](#chapter-06)
7. [Chapter 07](#chapter-07)
8. [Chapter 08](#chapter-08)
9. [Chapter 09](#chapter-09)
10. [Chapter 10](#chapter-10)

<!-- /code_chunk_output -->

## Chapter 01

### page 17 review question 6: What are CASE tools used for?

`CASE(Computer-Aided Software Engineering, 计算机辅助软件工程)`是以计算机为工具，辅助人在特定应用领域内完成任务的理论、方法和技术，`CASE`可以用于提升软件工程的沟通效率、协作效率、开发效率和产品质量等。

## Chapter 02

### page 41 review question 10: What are enterprise resource planning (ERP) systems?

`ERP(Enterprise Resource Planning, 企业资源计划)`是一个将企业内外部资源全面整合的系统，用于辅助管理企业的人（人力）、财（财务）、物（物资生产）、供（供应链）、销（销售）、存（库存）的一套企业信息系统。

## Chapter 03

### page 87 review question 22: What is work breakdown structure (WBS), and when should it be used?

`WBS(Work Breakdown Structure, 工作任务分解)`是指将项目工作分解成更小的活动，这些更小的活动具有以下特征：

1. 可交付：每个活动包含一个可交付的或明确的成果
1. 可分派：每个活动可分配给一个人或一个小组
1. 可控制：每个活动有一个负责人负责监督和控制

当需要有效地执行和控制项目工作，达到成本、时间、质量、范围相均衡的情况时，需要使用`WBS`。

## Chapter 04

### page 118 question 1: What kinds of information should be sought in interviews?

面谈时，需要获取面谈对象的观点，以及他们对系统当前状态、组织与个人目标、非常规程序的感受。

### page 119 question 19: What are the two basic question types used on questionnaires?

1. 开放式（open-ended）：面谈对象对问题可以进行不受限制的答复
1. 封闭式（closed）：面谈对象对问题只能在提供可选择项中选择其中的一个或多个进行答复

## Chapter 05

### page 142 question 8: What is text analytics?

对非结构化的文本数据进行分析，以协助进行下一步的信息挖掘、信息检索等应用。

## Chapter 06

### page 174 question 10: What are the four core practices of the agile approach?

1. 快速发布/简短发布
1. 保证休息/每周工作40小时
1. 用户参与/长期驻留一个用户代表
1. 结对编程

### page 174 question 15: What is Scrum?

`Scrum`是敏捷方法的一种，`Scrum`的特征有：

+ `Scrum`实践团队工作，类似橄榄球赛的团队一样，团队成员认识到项目的成功最重要，个人成功是次要的
+ `Scrum`注重行动并持续改进，以一个概要计划开始项目并不断地根据进展改变，类似橄榄球赛会采取一个总体策略一样
+ `Scrum`有一个严格的时间框架`Sprint`（2~4周一个`Sprint`），类似橄榄球赛对比赛时间有严格限制一样

### page 174 question 24: What does DevOps stand for?

1. `DevOps`是一种文化：强调开发运维一体化、强调自动化、强调协作、强调持续改进
1. `DevOps`是一系列的方法及其相应的工具链：自动化、持续集成/持续交付/持续部署、持续监控、持续迭代

## Chapter 07

## Chapter 08

## Chapter 09

## Chapter 10

### page 284 question 2: Describe the difference between a class and an object

1. 类是静态的，对象是动态的
1. 类是对象的模板，对象是类的实例

### page 285 question 6: What is UML?

`UML (Unified Modeling Language, 统一建模语言)`是一种由一整套图表组成的标准化的、用于系统分析与设计的、面向对象的建模语言，`UML`的一种重要特征是“用例驱动”。

### page 285 question 13: What does a use case model describe?

1. 用例模型包括用例图和用例场景，一张用例图包含多个用例，一个用例对应一个用例场景
1. 用例模型描述用户如何使用系统，即`actor`发出一个`event`，`event`触发一个`usecase`，`usecase`执行该`event`触发的行为
1. 用例模型关注系统做什么，而不是关注系统怎么做

### page 285 question 14: Would you describe a use case model as a logical or physical model of the system? Defend your answer in a paragraph.

### page 285 question 25: What are the two categories of relationships between classes?

### page 285 question 28: What does a state diagram depict?
