# 建立环境

## 任务和指引

1. 建立`editor`环境：<https://gitlab.com/arm_courses/sr/-/blob/master/Expertiments/Expt_Guides/SR-E02_Guide/SR-E02_guide.md>
1. 学习`markdown`语法：<https://gitlab.com/arm_awesomes/infrastructure/-/tree/master/docs/writing/lml/markdown/tutorial>
1. 使用`markdown`撰写学习笔记（以下主题中选择一个主题）：
    1. 计算机组成原理的主要知识点
    1. 操作系统的主要功能
    1. 编程语言的分类（按不同的维度划分）
