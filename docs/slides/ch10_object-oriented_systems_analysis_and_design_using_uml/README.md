---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Object-Oriented Systems Analysis and Design Using UML_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 基于UML的面向对象系统分析与设计

---

## 提纲Outline

1. 面向对象概念
1. UML模型图
1. UML实践

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 面向对象概念

---

+ `object`：需要被描述的实体
+ `class` and `object`
    1. `object`: `data` + `behavior`
    1. `class` is the template of `object`, `object` is a instance of `class`
+ characteristics of `OO`
    1. 继承
    1. 封装
    1. 多态

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## UML模型图

---

+ `UML(Unified Modeling Language, 统一建模语言)`由`Rational Software`于1994-1995开始开发，1997被`OMG(Object Management Group)`采纳
+ `UML`由事物/元素、关系和图组成
+ `UML`由`usecase`驱动

---

![height:600](./resources/image/fig10_04.jpg)

---

### 常用的模型图

---

![height:600](./resources/image/fig10_05.jpg)

---

### Usecase Diagram用例图

1. 描述用户如何使用系统：`actor`发出一个`event`，`event`触发一个`usecase`，`usecase`执行该`event`触发的行为
1. 一个`usecase`记载一个单独的事务或事件
1. 一张`usecase diagram`里包含一个或多个`usecase`，每个`usecase`对应一个`usecase scenario用例场景`

---

![height:600](./resources/image/fig10_06.jpg)

---

![height:600](./resources/image/fig10_07.jpg)

---

### Activity Diagram活动图

1. 描述用例的流程，每个`usecase`可对应一张`activity diagram`

---

![height:600](./resources/image/fig10_08.jpg)

---

![height:600](./resources/image/fig10_09.jpg)

---

### Sequence Diagram顺序图

1. 描述用例的流程的时序、类的实例之间的动态关系
1. 每个`usecase`对应一张或多张`sequence diagram`

---

![height:600](./resources/image/fig10_10.jpg)

---

![height:600](./resources/image/fig10_11.jpg)

---

### Class Diagram类图

1. 描述类、类与类的关系

---

![height:600](./resources/image/fig10_13.jpg)

---

![height:600](./resources/image/fig10_14.jpg)

---

### State Diagram状态图

1. 描述类的实例的状态的转移路径
1. 每个类可以创建一个`state diagram`
1. `event`致使对象的状态发生转移

---

![height:400](./resources/image/fig10_22.jpg)

<!--

---

![height:400](./resources/image/fig10_12.jpg)

-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## UML实践

---

1. 识别系统的业务需求（业务目标），定义用户需求（用例模型和用例场景），与用户确认用户需求
1. 根据用例场景导出业务流程（活动图、顺序图），与领域专家验证过程和交互，与用户确认业务流程
1. 开发类图
1. 开发某些类的状态图
1. 导出类（及其属性和方法）
1. 用文档详细记录

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
