---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "**_Project Management_**"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 项目管理Project Management

---

## 提纲Outline

1. 什么是项目
1. 启动项目
1. 可行性分析
1. 确定系统需求
1. 成本与效益
1. 时间与活动
1. 进度安排
1. 估算和预算、风险管理、时间管理、成本管理
1. 团队管理

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 什么是项目

---

+ 项目是为完成 **_某一独特的产品或服务_** 所做的 **_一次性努力_**
+ 项目的典型特征
    1. 一次性
    1. 独特性
    1. 目标的明确性
    1. 组织的临时性和开放性

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 项目启动

---

+ 提出系统项目的两大主要原因：
    1. 求助于系统解决`problem`
    1. 新系统带来改进的机会
+ 发现`problem`/识别`problem`
    1. 内部信号
    1. 外部信号

---

### 定义`Problem`

+ 定义`problem`是确定系统需求的基础
+ 定义目标（`business requirements`的核心）
+ 定义`problem`在完成用户面谈、观察和文档分析后产生

---

### `Problem` - `Requirement` - `Solution` - `Testing`

---

### 选择项目

+ 选择某个项目的基本理由：该项目可以解决某个问题，或者能够带来改进
+ 选择某个项目需要考虑的其他问题：
    1. 是否得到管理层的支持？
    1. 执行时间安排是否合理？
    1. 是否有利于提高组织战略性目标的达成？
    1. 资源是否切合实际？
    1. 此项目的机会成本是否合理？

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 可行性分析

---

1. 技术可行性
1. 经济可行性：时间、资金
1. 运营可行性

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 确定系统需求

---

![height:600](./resources/image/fig03_05.jpg)

---

![height:600](./resources/image/fig03_06.jpg)

---

1. `IaaS(Infrastructure as a Service)`/`HAAS(Hardware as a Service)` ==> 面向系统管理员
1. `PaaS(Platform as a Service)` ==> 面向程序员
1. `SaaS(Software as a Service)` ==> 面向用户

---

### 自研、外包还是购买

+ 定制
    1. 自研
    1. 外包
+ 购买
    1. `COTS(Commercial Off-The-Shelf)`
    1. `SaaS`

---

![height:600](./resources/image/fig03_08.jpg)

---

### 评估候选合作商

---

![height:600](./resources/image/fig03_09.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 成本与效益

---

+ 是否继续执行项目主要取决于成本效益分析，而不是信息需求
+ 预测模型：主要依据历史数据
+ 有形与无形：
    1. 有形：可以使用金钱进行度量
    1. 无形：难以使用金钱进行度量
+ 成本效益比较
    1. 收支平衡分析法
    1. 投资回报分析法

---

![height:600](./resources/image/fig03_10.jpg)

---

![height:550](./resources/image/fig03_11.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 时间与活动

---

### 工作分解结构WBS(Work Breakdown Structure)

+ 特征：
    1. 可交付：每个活动包含一个可交付的或明确的成果
    1. 可分派：每个活动可分配给一个人或一个小组
    1. 可控制：每个活动有一个负责人负责监督和控制
+ 要求：
    1. 每个活动 **不必要求** 花费同样的时间或同样的人力
    1. 所有活动相加 **要求** 等于100%的工作
+ 方法：
    1. 结构分解
    1. 过程分解

---

![height:600](./resources/image/fig03_12.jpg)

---

### 时间估算技术

1. 经验法：同类开发经验
1. 类比法：类似开发经验
1. 三点估算法：$E = (a + 4 \times m +b) \div 6$
1. 功能点分析法
1. 多因素估算法

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 项目进度计划

---

1. 进度计划的活动
    1. 工作分解结构
    1. 估算活动时间
    1. 活动编排调度：前置后置、串行并行
1. 进度计划的工具
    1. `Gantt Charts甘特图`
    1. `PERT(Program Evaluation and Review Technique, 计划评审技术)`

---

### Gantt Charts

1. 横向：时间
1. 纵向：活动描述
1. 条形：活动
1. 条形长度：活动所需时间

---

![height:600](./resources/image/fig03_15.jpg)

---

### PERT

+ 形状：
    1. 圆形：`event事件`
    1. 实线箭头线段：`activity活动`
    1. 虚线箭头线段：`pseudo-activity哑活动/虚拟活动`（仅表示前置后置关系的活动）
    1. 箭头方向：前置事件指向后置事件
+ 关键路径：所需时间最长的路径
    1. 关键路径上的任一活动延迟将导致整个项目的延迟

---

![height:600](./resources/image/fig03_16.jpg)

---

### PERT vs Gantt Charts

+ PERT的优势
    1. 逻辑准确：确定前置后置顺序
    1. 关键路径：确定应重点关注的活动
+ Gantt Charts的优势
    1. 直观

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 估算和预算、风险管理、时间管理、成本管理

---

+ 预算：人力成本、软硬件成本
+ 预算是项目费用支出的上限

---

![height:600](./resources/image/fig03_20.jpg)

---

+ 风险：未来可能发生的、会造成损失的事件
+ 风险管理：可以使用鱼骨图系统地列出所有存在的风险

---

![height:600](./resources/image/fig03_21.jpg)

---

+ 时间加速法：通过增加投入缩短活动时间

---

![height:600](./resources/image/fig03_22.jpg)

---

+ `EVM(Earned Value Management, 挣值管理法)`：根据当前实际情况计算成本偏差和进度偏差，并预估完成项目所需的实际预算
+ 4个关键指标：
    1. `BAC(Budget At Completion, 完工预算)`：项目计划的预算
    1. `PV(Planned Value, 计划价值)`：截止到某时间点计划完成工作量的价值（用货币体现），即，按时间进度计划，原本应完成的价值
    1. `EV(Earned Value, 挣值)`：截止到某时间点实际已经完成工作量的价值（用货币体现），即，实际上，该时间点已完成的价值
    1. `AC(Actual Cost, 实际成本)`：截止到某时间点实际已经发生的成本

>[详解挣值管理(EVM)](https://zhuanlan.zhihu.com/p/33925657)

---

![height:600](./resources/image/fig03_25.jpg)

---

+ 4个指标
    1. 成本偏差：$CV = EV - AC$
    1. 进度偏差：$SV = EV - PV$
    1. 成本绩效指数：$CPI = EV/AC$
    1. 进度绩效指数：$SPI = EV/PV$
+ 2个预估成本
    1. 完工尚需成本：$ETC = (BAC - EV) \div CPI$
    1. 预计完工成本：$EAC = AC + ETC$

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 团队管理

---

+ 理想的团队成员：经验丰富、工作积极、相互信任、能力互补
+ 显式和隐式的团队规范
+ 激励团队成员：`Maslow's hierarchy of needs马斯洛需求层次理论`

---

![height:600](./resources/image/Maslow's_Hierarchy_of_Needs.jpg)

---

### 项目章程

+ 项目章程应以书面形式描述
+ 项目章程描述的实质是交付物和交付时间
+ 项目章程是甲乙双方合同的一部分

---

1. **requirement** : what does the user expect of the project?
1. **scope** : what is the scope of the project?
1. **deadline** : what is the estimated project timeline?
1. **deliverables/production** : what are the project deliverables?
1. **acceptance criteria** : who will evaluate the system and how will they evaluate it?
1. **methods** : what analysis methods will the analyst use to interact with users?
1. **participants** : who are the key participants?
1. **training** : who will train the users?
1. **operation and maintenance** : who will maintain the system?

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
