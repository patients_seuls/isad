---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Information Gathering: Interactive Methods_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 信息收集：交互式方法

---

## 提纲Outline

1. （一对一）面谈
1. JAD会议(Joint Application Design, 联合应用设计)
1. 调查问卷

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 面谈Interviewing

---

+ 面谈：一种一对一、面对面的会谈，目的明确，一般采用问与答的形式
+ 准备面谈的5个步骤：
    1. 阅读背景材料
    1. 确定面谈目标
    1. 确定面谈对象
    1. 预约面谈对象
    1. 确定问题的种类和结构

---

### 问题的类型

1. 开放式问题
    + 优点：额外的信息
    + 缺点：不相关、易失控
1. 封闭式问题
    + 优点：切中要点
    + 缺点：难以得到丰富的细节（额外的信息）
1. 追问式问题probe/follow-up：紧接上一条问题进行追问
    + 优点：额外的信息
    + 缺点：易引起反感

---

![height:600](./resources/image/fig04_05.jpg)

---

### 问题的结构

1. 金字塔结构：特殊问题/封闭式问题 --> 一般问题/开放式问题
1. 漏斗结构：一般问题/开放式问题 --> 特殊问题/封闭式问题
1. 菱形结构：特殊问题/封闭式问题 --> 一般问题/开放式问题 --> 特殊问题/封闭式问题

---

### 完整的面谈流程

1. 准备面谈
1. 面谈并记录
1. 整理并书面化面谈记录
1. 分析面谈记录
1. 面谈报告

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## JAD

---

+ `JAD`是一个多方的、结构化的、正式的会议
+ `JAD`是范围广的、简便的`workshop(专题讨论会)`，也是分析人员与客户代表之间一种很好的合作办法，并能由此拟出需求文档的底稿

>1. <https://baike.baidu.com/item/JAD>
>1. <https://en.wikipedia.org/wiki/Joint_application_design>

---

### JAD会议的要点

1. 一位地位较高的人做主办人
1. 一位善于协调的人做主持人
1. 一位或两位观察员
1. 一位记录员
1. 至少一位分析员全程参与
1. 8～12位不同阶层的用户参加JAD会议
1. 2～4天的脱产会议

---

### JAD会议的议题

1. 关于系统项目的所有主题
1. 每个主题还要提问：who, what, how, where, why

---

### JAD会议的优势

1. 节省信息收集的总时间
1. 用户参与、用户反馈

---

### JAD会议的劣势

1. 与会者必须保证2～4天的全程参与
1. 会前必须准备充分、会后必须及时完成后续任务
1. 组织必须全力支持`JAD`会议这种方式

---

### JAD会议成功的前提条件

1. 准备充分、议题清晰
1. 与会人员重视
1. 用户信息化水平高

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 问卷调查

---

+ 问卷调查：通过发放、回收调查问卷，快速收集大量数据的方法
+ 面谈的问题与问卷的问题的区别：面谈的问题允许有额外的、临时补充的`context`

---

### 问卷调查的关键点

1. 清晰地了解想从问卷调查中得到什么
1. 清晰地了解问卷调查在当前时机下是否合适
1. 精心地制作一份有效的问卷调查表
1. 选择合适的问卷发放和回放机制

---

### 有效的问卷调查表

1. 准确地选择问题类型（开放式问题？封闭式问题？单选？多选？）
1. 有效地确定问题顺序
1. 准确地使用一致的、用户的专业术语
1. 准确地使用度量标度
1. 准确地选择调查对象，有效地区分有效回卷还是无效回卷

---

![height:600](./resources/image/fig04_13.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
