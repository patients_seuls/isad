---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 系统、角色和开发方法Systems, Roles, and Development Methodologies

---

## 提纲Outline

1. 哲学、科学、工程
1. 信息、系统、信息系统
1. 分析、设计
1. 需求分析师、产品经理、系统分析员、系统架构师

---

1. 组织中的系统分析与设计需求
1. 系统分析员的众多角色
1. 3种重要的软件工程方法： `SDLC`/`waterfall`、`Agile`、`Object-Oriented`
1. `agile`, `continuous integration`, `continuous delivery`, `continuous deployment`, `DevOps`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 哲学、科学、工程

---

+ 哲学philosophy：研究普遍的、基本问题的学科，包括存在、知识、价值、理智、心灵、语言等领域
+ 科学science：形式科学/思维科学/逻辑科学、自然科学、社会科学
+ 工程engineering：由 **_一群（个）人_** 为达到某种目的，在 **_一个较长时间周期内_** 进行 **_协作（单独）活动_** 的 **_过程_**

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 信息、系统、信息系统

---

1. 信息：信息是用以消除随机不定性的东西
    + 数据：数据是信息的逻辑载体，数据包含着数字化的信息，数据的表现形式可以是数字、符号、文字、图片、音频、视频等
    + 信号：数据在传输过程中的表现形式
1. 系统：有一定结构的、有一定功能的、由若干元素组成的整体
1. 信息系统：输入数据、加工处理后产生信息的系统
    + 广义：任意具有`input`、`process`、`output`信息能力的系统
    + 狭义：用以改进组织的过程的计算机化的应用系统

:point_right: 信息是一种关键资源 :point_left:

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 分析、设计

---

+ 分析：把事物、概念分解成较简单的组成部分，分别加以考察，找出各自的本质属性及彼此间的联系
+ 设计：
    1. 定下计谋
    1. 在工作或工程开始之前，预先制定方案，规划蓝图等

:point_right: :book: PVII 系统分析与设计是一个众多工具的使用与分析员特有的才智相结合的过程 :point_left:

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 需求分析师、产品经理、系统分析员、系统架构师、项目管理师

---

+ 需求分析师
+ 产品经理
    1. 在互联网企业：$\approx$ 需求分析师 + 项目管理师
    1. 在小型传统`toB`软件企业：$\approx$ 项目管理师
+ 系统分析员/系统分析师
    1. 早期：$\approx$ 需求分析师 + 系统架构师
    1. 当下：$\approx$ 需求分析师

><https://zhuanlan.zhihu.com/p/26989814>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 系统分析员

---

### 系统分析员的众多角色

1. 系统分析员作为顾问（外聘顾问）
1. 系统分析员作为支持专家（内部顾问）
1. 系统分析员作为变更代理（需求分析师 + 系统架构师）

---

### 系统分析员的品质

1. 乐于解决问题的人
1. 善于沟通交流的人
1. 丰富编程经验的人
1. 健康道德准则的人

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 系统开发生命周期模型SDLC

---

### SDLC是什么

+ `SDLC`
    1. Software Development Life Cycle
    1. System Development Life Cycle
+ 严格按照各个阶段的顺序依次完成
+ 也称为瀑布模型`waterfall model`或结构化模型`structured model`

---

### SDLC的各阶段

1. 问题定义：标识问题、机会和目标
1. 需求收聚：确定人的信息需求
1. 需求分析：分析系统需求
1. 系统设计：设计推荐的系统
1. 系统开发：软件开发和编制文档
1. 软件测试
1. 价值实现与二次迭代
    1. 系统运维与业务运营：系统价值实现
    1. 评估升级：二次迭代

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 敏捷模型Agile

---

### 敏捷是什么

+ 敏捷方法是一类软件工程模型，包括`Scrum`、`XP`（极限编程）、`Crystal Methods`、`FDD`（特性驱动开发）等
+ 交互式和增量式的软件工程模型

---

### 4个价值观

+ 交流、简化、反馈和勇气 :question: :book: P10
+ 尽管右项有其价值，我们更重视左项的价值
    1. 个体和互动 高于 流程和工具
    1. 工作的软件 高于 详尽的文档
    1. 客户合作 高于 合同谈判
    1. 响应变化 高于 遵循计划

>+ [敏捷软件开发宣言](https://agilemanifesto.org/iso/zhchs/manifesto.html)
>+ [Manifesto for Agile Software Development](https://agilemanifesto.org/iso/en/manifesto.html)

---

### 4个核心实践

1. :star2: short release：`Scrum`的一个`sprint`约2至4周
1. 现场预留一名客户（客户代表）
1. 结对编程pair programming
1. 每周工作40小时（每天8小时）

---

### 敏捷过程

![height:550](./resources/image/fig01_04.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 面向对象模型Object-Oriented

---

### 面向对象模型是什么

+ `OO`将系统的组成部分剖析成一个个对象，对象 = 数据 + 行为
+ `OOA`和`OOD`采用`UML`建模语言

---

### OO的过程

![height:550](./resources/image/fig01_05.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## CASE工具CASE Tools

---

+ Computer-Aided Software Engineering
+ 计算机辅助技术(Computer-Aided Technology/Computer-Aided X)是以计算机为工具，辅助人在特定应用领域内完成任务的理论、方法和技术
    1. 计算机辅助设计(CAD, Computer-Aided Design)
    1. 计算机辅助制造(CAM, Computer-Aided Manufacturing)
    1. 计算机辅助教学(CAI, Computer-Aided Instruction)
    1. 计算机辅助质量管理(CAQ, Computer-Aided Quality control)
    1. ......

---

### 常用CASE工具

1. 图例建模
1. 源码辅助
1. 配置管理
1. 数据库建模
1. ......

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 敏捷、持续集成、持续交付、持续部署和开发运维一体化Agile, Continuous Integration, Continuous Delivery, Continuous Deployment, DevOps

---

![height:600](./resources/image/DevOps-process-flow.png)

---

### DevOps

+ `DEVelopment` + `OPerationS`
+ 一种文化、一种方法、一套工具链、一组过程

---

![width:1200](./resources/image/DevOps-Tools-DevOps-Tutorial-Edureka-1.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 开源软件Open Source Software

---

### 开源软件

+ 开放源代码
+ 开源 != 免费

---

### 开源软件的利润点

1. 形象
1. 服务
    1. 软件运营服务
    1. 培训认证服务

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
