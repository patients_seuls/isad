---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Using Data Flow Diagrams_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 使用数据流图Using Data Flow Diagrams

---

## Outlines

1. DFD概述
1. 绘制DFD
1. 逻辑DFD与物理DFD

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## DFD概述

---

+ `DFD(Data Flow Diagram, 数据流图)`：从数据传递和加工角度，以图形方式来表达系统的逻辑功能、数据在系统内部的逻辑流向和逻辑变换过程，是结构化系统分析方法的主要表达工具及用于表示软件模型的一种图示方法
+ `DFD`的优点
    1. 隐藏技术实现细节
    1. 通过`DFD`将当前系统知识传达给用户（用户不关心技术实现细节）
    1. 帮助理解系统与子系统的相互关系
    1. 帮助判断是否已定义好必要的数据和过程

><https://zh.wikipedia.org/zh-cn/%E8%B3%87%E6%96%99%E6%B5%81%E7%A8%8B%E5%9C%96>

---

### DFD的元素

+ `IPSO(Input Process Storage Output)`
    1. `input/output`：
        1. `data flow`：箭头，`名词`命名
        1. `entity/data source/data destination`：双重正方形，`名词`命名
    1. `process`：圆角矩形
        1. 系统或子系统`名词`命名，具体处理过程`动词-形容词-名词`命名
        1. 唯一的`ID`，同时该`ID`能指出它所在的层次
    1. `storage`：开口矩形，`名词`命名，唯一的`ID`

---

![height:600](./resources/image/fig07_01.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

### 绘制DFD

---

### DFD的绘制流程

1. 绘制`上下文关系图`/`顶层DFD`：不显示任何具体的过程或数据存储，仅展示系统与外部实体的`IO`关系
1. 绘制`0层DFD`
1. 绘制`1层DFD`

---

![height:680](./resources/image/fig07_03.jpg)

---

![height:680](./resources/image/fig07_04.jpg)

---

### DFD的基本规则

1. 至少有一个`process`，且不能有任何孤立的对象，也不能有连接到自己的对象
1. 每个`process`至少有一个输入流、一个输出流
1. 每个`storage`至少与一个`process`连接，且`storage`之间不能直接连接
1. `entity`之间不能直接连接，`entity`与`storage`之间不能直接连接
1. 每张`DFD`的`process`数至多包含9个`process`（`上下文关系图`用`0`标识，`上下文关系图`以下的`DFD`从`1`至`9`）

---

![height:680](./resources/image/fig07_05.jpg)

---

![height:680](./resources/image/fig07_06.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 逻辑DFD与物理DFD

---

+ 逻辑`DFD`关注业务及其运营方式，不关注如何构造系统，即，描述发生的业务事件，以及每个事件要求的数据和产生的数据
+ 物理`DFD`关注系统的实现，包括硬件、软件、文件和人等

:point_right: 逻辑模型反映业务，物理模型反映系统 :point_left:

---

![height:650](./resources/image/fig07_07.jpg)

---

![height:680](./resources/image/fig07_08.jpg)

---

![height:680](./resources/image/fig07_09.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
