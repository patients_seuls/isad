---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 理解并建模组织系统Understanding and Modeling Organizational Systems

---

## 提纲Outline

1. 组织作为一个系统
1. 图形化描述系统的方法
1. 用例建模
1. 管理的层级、组织文化

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 组织作为一个系统

---

+ 组织是由一系列更小的、相互关联的、行使具体职能的系统（部门、单位或分部等）构成的大系统
+ 通过系统原理洞察组织的运作方式
+ 所有的系统和子系统都是相互关联和相互依赖的

---

![width:1100](./resources/image/fig02_01.jpg)

---

![height:600](./resources/image/fig02_02.jpg)

---

![height:600](./resources/image/fig02_03.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 图形化描述系统的方法

---

### 上下文数据流图Context-Level Data Flow Diagram

+ 上下文数据流图：最高抽象级的数据流图，表明系统的范围的一种方法
    1. 圆角矩形：处理
    1. 正方形：外部实体
    1. 箭头：数据流

---

![height:600](./resources/image/fig02_04.jpg)

---

![height:600](./resources/image/fig02_05.jpg)

---

### 实体-关系图Entity-Relationship Diagram

+ 实体：构成系统的元素或事件
+ 关系：实体间的相互作用

---

### 用例模型Use Case Model

+ 用例模型是`UML`一部分，也可单独使用
+ `use case`中的`use`是`noun.`
+ 用例模型描述一个系统做什么，而不描述怎么做
+ 用例模型从系统外部的用户维度反映系统（即，系统需求/用户需求）
+ 用例模型包括用例图`use case diagram`和`use case scenario`/`use case specification`/`use case description`

---

#### 用例图Use Case Diagram

---

##### 符号类型Symbols

1. 参与者`actor`
1. 用例`use case`：用例名称通常使用“动宾短语”形式
1. 连接线/关系`relationship`
1. 系统边界/系统范围`system border`

---

##### 连接线/关系

1. 交流`communicate`/`associate`：无方向箭头，连接`actor`和`use case`，表示 **_执行关系_**
1. 包含`include`：单方向箭头、箭头指向`common use case`，连接两个`use case`，表示 **_分解或复用关系_**
1. 扩展`extend`：单方向箭头、箭头指向`basic use case`，连接两个`use case`，表示 **_可选或异常关系_**
1. 泛化`generalize`：单方向空心箭头、箭头指向`general thing`，连接两个`use case`或两个`actor`，表示 **_一般<->特殊关系_**

---

#### 用例的抽象级别

+ 创建不同抽象级别的用例

---

#### 用例场景Use Case Scenario

+ 每个`use case`都有一个`use case scenario`
+ `use case scenario`的基本要素：
    1. `name` and `ID`
    1. `actor`
    1. 三事件：前置条件、触发条件、后置条件
    1. 三步骤/三流程：主流程、可选流程/扩展流程、异常流程

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 管理的层次/层级、组织文化

---

1. 管理的层级
    + 运营控制管理（基层管理）、管理规划和控制管理（中层管理）、战略管理（高层管理）
    + 每个层级承担不同的职责
    + 每个层级需要获取的信息的类型各不相同
1. 组织文化
    + 组织文化影响如何使用信息系统
    + 信息系统影响组织文化的演进

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
