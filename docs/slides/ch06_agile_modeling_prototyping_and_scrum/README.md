---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Agile Modeling, Prototyping, and Scrum_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 敏捷建模、原型法和Scrum Agile Modeling, Prototyping, and Scrum

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 原型法

---

+ 通过原型，与用户确认需求的吻合程度
+ 原型法可以以较低的代价和较小的破坏性重定计划
+ 用户在原型法的角色：忠实的参与者

---

### 原型的类型

1. `拼凑原型patched-up prototype`：可运行的、具备所有必要特性但又低效的原型
1. `非操作原型nonoperational prototype`：为试验设计方案的某些方面而建立的一个不可运行的原型
1. `系列首发原型first-of-a-series prototype`：系统的第一个可运行原型
1. `精选特性原型selected features prototype`：具备部分特性的、可运行的原型

---

### 原型的划分角度

1. 是否可运行
1. 是否电子化
1. 是否可演进
1. 是否全特性
1. 是否仅演示
1. 是否高保真

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 敏捷方法

---

+ 敏捷方法是一组以用户为中心的、解决需求与开发之间矛盾的、一系列的软件开发方法
+ 以前，需求由计算机专家设计出来，现在，需求由需求专家从用户那里分析出来
+ 敏捷方法的4大价值：交流、简化、反馈和勇气

---

### 敏捷原则

1. 持续地、及早地交付：我们最重要的目标，是通过持续不断地及早交付有价值的软件使客户满意
1. 迎接需求变化：欣然面对需求变化，即使在开发后期也一样。为了客户的竞争优势，敏捷过程掌控变化
1. 经常性、短周期交付：经常地交付可工作的软件，相隔几星期或一两个月，倾向于采取较短的周期
1. 用户持续参与：业务人员和开发人员必须相互合作，项目中的每一天都不例外

---

1. 信任并激励个体：激发个体的斗志，以他们为核心搭建项目。提供所需的环境和支援，辅以信任，从而达成目标
1. 提倡面对面交谈：不论团队内外，传递信息效果最好效率也最高的方式是面对面的交谈
1. 可工作是首要标准：可工作的软件是进度的首要度量标准
1. 持续开发：敏捷过程倡导可持续开发。责任人、开发人员和用户要能够共同维持其步调稳定延续

---

1. 追求卓越：坚持不懈地追求技术卓越和良好设计，敏捷能力由此增强
1. 简洁为本：以简洁为本，它是极力减少不必要工作量的艺术
1. 自组织团队：最好的架构、需求和设计出自自组织团队
1. 定期反思和调整：团队定期地反思如何能提高成效，并依此调整自身的举止表现

---

### 敏捷方法的活动、资源和实践

---

#### 敏捷方法的活动

+ 4个基本活动：倾听、设计、编码、测试

---

+ 编码：
    1. 有一个想法，马上进行编码，对代码进行测试，然后判断这个想法是否合理
    1. 代码可以用来传达原本模糊或未成形的思想
    1. 阅读他人的代码，可以获得一种新思想

---

+ 测试：
    1. 提倡并依赖于自动化测试
    1. 测试的短期目的和长期目的
        1. 短期目的：提升自信心，持续进行后续编码
        1. 长期目的：保持软件生命力

---

+ 倾听：
    1. 倾听编程伙伴的意见
    1. 倾听用户/客户的意见
+ 设计：
    1. 简洁的设计
    1. 灵活的、可演进的设计

---

#### 敏捷方法的资源

+ 4个资源控制元素：时间、成本、质量和范围
+ 时间：持续地、按时地交付
+ 成本：提升个体的生产力，不盲目增加人数、不盲目加班
+ 质量：不提倡调整内部质量属性，允许牺牲某些外部质量属性
+ 范围：
    1. 通过`用户故事user story`确定范围
    1. 为保证质量、成本、时间，可以按实际情况调整范围

---

![height:600](./resources/image/iron_triangle_paradigm_shift.png)

---

#### 敏捷方法的实践

+ 4个核心实践：简短发布、每周工作40小时、长期驻留一个用户、结对编程

---

![height:600](./resources/image/fig06_03.jpg)

---

### 敏捷建模

+ 建模步骤
    1. 倾听用户描述的用户故事
    1. 画出逻辑工作流模型，评价用户故事中表示的商业决策
    1. 根据该逻辑模型建立新的用户故事
    1. 开发一些演示原型，向用户阐述他们将拥有的界面
    1. 通过演示原型的反馈和逻辑工作流图开发系统，直至创建一个可运行模型
+ 用户故事不缺，需要选择几个用户故事，马上完成编程、测试并发布，然后迭代
+ 用户故事记录在纸质卡片或电子卡片上

---

![height:550](./resources/image/fig06_04.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Scrum

---

+ `Scrum`取自橄榄球赛的一种开始位置
+ `Scrum`实践团队工作，类似橄榄球赛的团队一样，团队成员认识到项目的成功最重要，个人成功是次要的
+ `Scrum`注重行动并持续改进，以一个概要计划开始项目并不断地根据进展改变，类似橄榄球赛会采取一个总体策略一样
+ `Scrum`有一个严格的时间框架`Sprint`（2~4周一个`Sprint`），类似橄榄球赛对比赛时间有严格限制一样
+ `Scrum`是一种敏捷方法，适用于需要持续行动的、较复杂的项目

---

### Scrum的角色

1. 产品负责人/产品所有者product owner：项目创建人，表达了对产品的愿景，负责项目的初始计划、产品发布和最终评估
1. `Scrum`主管`Scrum` master：充当团队的教练、知识渊博的顾问、经验丰富的开发者和协调者
1. 团队成员

---

### Scrum的核心概念

1. 产品待办列表product backlog
    + 列表项是由用户故事而设计的产品特性、其他交付物、待修复的`bug`、待编写的文档等
    + 按优先级排序，高优先级的位于上面
1. `Sprint`周期和`Sprint`待办列表`Sprint` backlog
    + `Sprint`周期定于2~4周之间，一般定为2周

---

![height:600](./resources/image/fig06_05.jpg)

---

![height:600](./resources/image/fig06_06.jpg)

---

### Scrum的其他特有特征

1. 规划会议`planning meeting`
1. 每日站立会议`daily stand-up meeting`
1. 燃尽图`burn-down chart`
1. `sprint`评审会议`sprint review`
1. 看板`kanban`/`board`

---

#### 规划会议

+ 规划会议的主题
    1. 产品负责人展示用户故事愿景列表中列出的特性列表，此时，团队成员则向他询问有关问题
    1. 估计完成这些特性所需的资源
    1. 工作分解成多个任务，对用户故事进行优先级排序，并承诺在当前`sprint`结束时应完成的任务
+ 资源估计的工具：计划扑克`planning poker` :book: P162

---

![height:600](./resources/image/fig06_07.jpg)

---

#### 每日站立会议

+ 持续几分钟至十几分钟，团队成员站立开会
+ `Scrum`每日站立会议主题
    1. 相互汇报上次会议后已完成的任务
    1. 相互汇报当前会议后应完成的任务
    1. 相互陈述接下来可能会遇到的困难

---

#### 燃尽图

+ 用于跟踪工作进度情况的一种方法
+ 每日更新
+ 图形元素
    1. 横轴：日期（`sprint`周期度量）
    1. 纵轴：剩余任务数或预估的尚需小时数
    1. 红色线条：剩余的工作小时数
    1. 黄色方条：剩余任务数

---

![height:600](./resources/image/fig06_08.jpg)

---

#### Sprint Review

+ 一个`sprint`结束后进行的回顾性的评审会议，阐明成绩和经验教训，通过说明什么做得好，什么做得不好，以便下一个`sprint`中可以改进
+ 主题：
    1. 复审已完成的工作，记录已完成的用户故事
    1. 记录未完成的任务/用户故事

---

#### 看板

+ 由丰田公司提出的概念和工具

---

![height:600](./resources/image/fig06_09.jpg)

---

### Scrum的优缺点

---

#### Scrum的优点

1. 快速产品开发
1. 面向用户
1. 鼓励团队工作
1. 比正式方法更加少的混淆
1. 灵活
1. 满足团队成员
1. 奖励较小但有意义的成就
1. 反馈
1. 可适应性

---

#### Scrum的缺点

1. 不适当地记录特性
1. 可能会发布有缺陷的产品
1. 过早发布产品给用户
1. 在压力下完成`sprint backlog`
1. 地理分散的团队很难进行工作（例如：站立会议）
1. 面对需要特殊技能的任务时可能面临挑战
1. 替换团队成员代价较大

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## DevOps

---

+ `DevOps` = `Development` + `Operations`
+ `DevOps`的目标：尽可能将`SDLC`的各阶级以自动化的方式融合在一起
+ `DevOps`是一种文化、一系列的方法及其相应的工具链

---

![height:600](./resources/image/fig06_10.jpg)

---

![height:600](./resources/image/DevOps-process-flow.png)

---

### CICD

+ `CI(Continuous Integration, 持续集成)`：频繁地（一天多次）将代码集成到主干
+ `CD(Continuous Delivery, 持续交付)`：频繁地将软件的新版本，交付给质量团队或者用户，以供评审，如果评审通过，代码就进入生产阶段
+ `CD(Continuous Deployment, 持续部署)`：代码通过评审以后，自动部署到生产环境，持续部署是持续交付的下一步

><https://www.ruanyifeng.com/blog/2015/09/continuous-integration.html>

---

![height:550](./resources/image/continuous_delivery_vs_continous_deployment.jpg)

><https://blog.crisp.se/2013/02/05/yassalsundman/continuous-delivery-vs-continuous-deployment>

---

![height:600](./resources/image/DevOps-Tools-DevOps-Tutorial-Edureka-1.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 敏捷方法与瀑布方法的比较

---

![height:600](./resources/image/fig06_11.jpg)

---

![height:600](./resources/image/fig06_12.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 组织革新的风险

---

![height:600](./resources/image/fig06_13.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
