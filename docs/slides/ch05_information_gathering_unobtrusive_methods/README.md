---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Information Gathering: Unobtrusive Methods_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 信息收集：非干扰性方法Information Gathering: Unobtrusive Methods

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 非干扰性方法/独立获取方法

---

+ 非干扰性方法：独立工作以发现收集信息
+ 完全使用非干扰性方法可能不足以充分地收集信息

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 采样

---

+ 采样：从某一种群中系统地选出一些有代表性的个体的过程
+ 采样的假设：通过仔细研究所选的个体，可以从整体上提示种群的信息
+ 采样的必要性：
    1. 节约成本
    1. 加快收集过程
    1. 提高效率
    1. 减少偏差

---

+ 采样的步骤：
    1. 确定要收集的或要描述的数据
    1. 确定采样种群
    1. 选择采校类型：是否基于概率、是否无约束条件
    1. 决定采样规模

---

![height:600](./resources/images/fig05_01.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文档分析法

---

### 定量文档分析

+ 定量文档分析的对象：报表（决策报表、业绩报表）、记录、表单等
+ `报表report`：在原始数据的基础上进行计算后得到的表格，用于向上级报告情况的表格
+ `记录record`：记录业务过程中发生的事件的详细清单的表格
+ `表单form`：在数据收集过程中用于填写数据的表格

---

![height:600](./resources/images/fig05_03.jpg)

---

![height:600](./resources/images/fig05_04.jpg)

---

![height:600](./resources/images/fig05_05.jpg)

---

### 定性文档分析

+ 定性文档分析的对象：电子邮件、备忘录、留言板、工作区、Web、程序指南、政策手册等
+ 重点分析情感、情绪、动机等方面的信息
+ 备忘录：了解一个组织成员对价值、态度和信念的看法
+ 公告板或工作区的标语或海报：了解组织的主流文化
+ Web站点：技术、美学、管理
+ 使用手册：规范、态度
+ 政策手册：价值观、态度和信念

---

### 文本分析工具

+ 辅助分析非结构化数据

---

![height:600](./resources/images/word_cloud.jpeg)

---

![height:600](./resources/images/knowledge_graph.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 观察法

---

### 观察决策者的行为

+ 观察的对象：观察决策者、决策者所处的环境以及他们与环境的交互
+ 观察的目的：力图洞察决策者实际上做什么，而不仅仅是决策者记录了什么、解释了什么
+ 观察的工具：`剧本play script`
+ 观察法的缺点：耗时

---

![height:600](./resources/images/fig05_09.jpg)

---

### 观察环境

+ 观察的对象：决策者的办公室
+ 观察的工具：`结构化环境观察STROBE(STRuctured OBservation of the Environment)`，通过结构化的观察要素定性分析决策者的特征
+ `STROBE`的要素：办公室位置、办公桌布置、固定物品、道具、外部信息源、办公室照明和色调、决策者的衣着

---

![height:600](./resources/images/fig05_10.jpg)

---

![height:600](./resources/images/fig05_11.jpg)

---

+ 通过`STROBE`方法验证其他方法收集的信息
    1. 钩号：肯定/支持叙述
    1. 叉号：否定/推翻叙述
    1. 椭圆：待进一步考证
    1. 方形：修改叙述
    1. 圆形：补充叙述

---

![height:600](./resources/images/fig05_12.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
