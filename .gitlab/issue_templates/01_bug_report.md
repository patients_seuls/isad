<!--
---
name: :bug: bug report
about: report a bug
---
🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅

oh hi there! 😄

to expedite issue processing please search open and closed issues before submitting a new one.

existing issues often contain information about workarounds, resolution, or progress updates.

🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅
-->

<!--
this is borrow from:

https://raw.githubusercontent.com/angular/angular/master/.github/ISSUE_TEMPLATE/1-bug-report.md

https://raw.githubusercontent.com/stevemao/github-issue-templates/master/bugs-only/ISSUE_TEMPLATE.md
-->

# :bug: Bug Report

## :incoming_envelope: Short Description

<!-- :memo: a clear and concise description of the problem... -->

## :camera_flash: Snapshoot

<!-- :memo: often a screenshot can help to capture the issue better than a long description. -->
<!-- :memo: upload a screenshot:-->

## :slightly_smiling_face: Expected Behavior

<!--- :memo: tell us what should happen -->

## :upside_down_face: Actual Behavior

<!--- :memo: tell us what happens instead of the expected behavior -->

## :angry: Is This a Regression?

<!-- :memo: did this behavior use to work in the previous version? -->

<!-- :memo: yes, the previous version in which this bug was not present was: .... -->

## :computer: Your Context (Environment)

<!--- :memo: How has this issue affected you? What are you trying to accomplish? -->
<!--- :memo: Providing context helps us come up with a solution that is most useful in the real world -->

<!--- :memo: Provide a general summary of the issue in the Title above -->

## :memo: Steps to Reproduce

<!--- provide a link to a live example, or an unambiguous set of steps to -->
<!--- reproduce this bug. Include code to reproduce, if relevant -->

1. <!-- :memo: step description-->
1. <!-- :memo: step description-->
1. <!-- :memo: step description-->
1. <!-- :memo: step description-->

## :hammer_and_wrench: Possible Solution
<!-- not obligatory, but suggest a fix/reason for the bug, -->

## :link: Anything Else Relevant?
<!-- please provide additional info if necessary. -->
